package com.dmitriy.azarenko.workwithfragments1;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

//как сделать метод для вызова надписи или цифр в разных текстах(секонд фрагмент,сет текст) если не 3 метода
//как сделать трансфер из одной системы исчисления в другую
// зачем делать невидимы контейнер, что такое fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        FirstFragment first_fragment = FirstFragment.newInstance();
        SecondFragment second_fragment = SecondFragment.newInstance();
        ft.add(R.id.first_container, first_fragment,FirstFragment.class.getSimpleName());
        ft.add(R.id.second_Container, second_fragment, SecondFragment.class.getSimpleName());
        ft.commit();


    }
}
