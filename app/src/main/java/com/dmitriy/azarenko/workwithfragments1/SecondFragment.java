package com.dmitriy.azarenko.workwithfragments1;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class SecondFragment extends Fragment {

    private EditText textBinTrans;
    private EditText textOctTrans;
    private EditText textHexTrans;


    public SecondFragment() {
        // Required empty public constructor
    }

    public static SecondFragment newInstance() {
        SecondFragment fragment = new SecondFragment();

        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, null);

         textBinTrans = (EditText) view.findViewById(R.id.editTextBibTrans);
         textOctTrans = (EditText) view.findViewById(R.id.editTextOctTrans);
         textHexTrans = (EditText) view.findViewById(R.id.editTextHextrans);







        return view;
    }


        public void setBin(String txt){


        String bin = Integer.toBinaryString(Integer.parseInt(txt));
            textBinTrans.setText(bin);



    }

//    public void setText(String txt){
//
//        textBinTrans.setText(txt);
//
//    }
    public void setText2(String txt) {

        textOctTrans.setText(txt);
    }
    public void setText3(String txt) {

        textHexTrans.setText(txt);
    }




}
