package com.dmitriy.azarenko.workwithfragments1;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class FirstFragment extends Fragment {


    private EditText textBin;
    private EditText textOct;
    private EditText textHex;


    public FirstFragment() {

    }


    public static FirstFragment newInstance() {
        FirstFragment fragment = new FirstFragment();

        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_first, null);


        textBin = (EditText) view.findViewById(R.id.editTextBin);
        textOct = (EditText) view.findViewById(R.id.editTextOct);
        textHex = (EditText) view.findViewById(R.id.editTextHex);

        Button btnBin = (Button) view.findViewById(R.id.button);
        Button btnOct = (Button) view.findViewById(R.id.button2);
        Button btnHex = (Button) view.findViewById(R.id.button3);


        btnBin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String binar = textBin.getText().toString();

                SecondFragment secfrag = (SecondFragment) getActivity().getSupportFragmentManager().findFragmentByTag(SecondFragment.class.getSimpleName());
                if (secfrag !=null){


               secfrag.setBin();
                }


            }
        });

        btnOct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SecondFragment secfrag = (SecondFragment) getActivity().getSupportFragmentManager().findFragmentByTag(SecondFragment.class.getSimpleName());
                if (secfrag !=null) {
                    secfrag.setText2("Second Button");
                }

            }
        });

        btnHex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SecondFragment secfrag = (SecondFragment) getActivity().getSupportFragmentManager().findFragmentByTag(SecondFragment.class.getSimpleName());
                if (secfrag !=null) {
                    secfrag.setText3("Third Button");
                }

            }
        });

        return view;
    }



}
